
#include <iostream>
using namespace std;
#include <ctime>
#include <cstdlib>
#include <vector>

int board(int n, char mark, int choice, int player, char* square);

//Function to generate random number for single player game mode
int random_num(int n)
{
    int outPut;
    srand(time(NULL)); //generates random seed val
    outPut = rand()%(n + 1); 
    return outPut;
    
}


int main()
{

  int n = 0,p;
  cout<<"Enter number of grids(n*n) you want to play with (3 or more) "<<endl;
  cin>>n;
  int a = n*n;
  char square[a];
  vector<int> v;

  for (int i = 0; i <= a; i++)
  {
     square[i] = '*';
  } 

  cout<<"Please type 1 for one player game "<<endl;
  cout<<"Please type 2 for two player game "<<endl;
  cin>>p;
 
  cout << "\n\n\tTic Tac Toe\n\n";

  cout << "Player 1 (X)  -  Player 2 (O)" << endl << endl;
  cout << endl;

  for (int j =1; j<= a; j++)
  {
    cout<<" ";
    if(j>9)
      cout<<j<<" |";
    if(j<=9)
      cout<<j<<"  |";  
    if(j % n == 0)
      cout<<endl<<endl;
  }  

  int player = 1,i,choice;
  int cnt = 0;
  char mark;

  if(p ==2)
  {
      do
      {
          cnt = cnt +1;
          player=(player%2)?1:2;

          cout << "\nPlayer " << player << ", enter a number:  ";
          cin >> choice;

          mark=(player == 1) ? 'X' : 'O';
          i = board(n, mark, choice, player, square);
          player++;

      } while(i == -1);

      if(i==1)
        cout<<"==>\aPlayer "<<--player<<" win ";
      else
      {
        cout<<"i is "<<i<<endl;
        cout<<"==>\aGame draw";
      }   

      cin.ignore();
      cin.get();
      return 0;
  }

  if(p ==1)
  {
     do
      {
          
          cnt = cnt + 1;
          player=(player%2)?1:2;

          if(player == 1)
          {
            cout << "\nPlayer " << player << ", enter a number:  ";
            cin >> choice;
            cout<<"1 choice  :"<<choice<<endl; 
          
            mark = 'X';
          }

          v.push_back(choice);

          if(player == 2)
          {
            choice = random_num(a);
            cout<<"choice  "<<choice<<endl;
            cout<<"size :"<<v.size()<<endl;
            
            for(int k = 0; k<=v.size(); k++)
            { 
              while(v[k] == choice )
                choice = random_num(a);
            }
            
            mark = 'O';
          }
          v.push_back(choice);

          for(int l = 0; l<v.size(); l++)
          {
            cout<<"Vector [i] "<< v[l];
          }
          i = board(n, mark, choice, player, square);
          player++;

      }while(i == -1);

      if(i==1)
        cout<<"==>\aPlayer "<<--player<<" win ";

      else
      {
        cout<<"i is "<<i<<endl;
        cout<<"==>\aGame draw";
      }   

      cin.ignore();
      cin.get();
      return 0;
  }

}

 int board(int n, char mark, int choice, int player, char* square)
{
        
    int a = n*n;
    
   
    if(square[choice] == 'X' || square[choice] =='O')
    {
      cout<<"seletion already taken"<<endl;
      cout<<"Invalid move ";

            player--;
            cin.ignore();
            cin.get();
    }
    else
      cout<<"choice   "<<choice<<endl;
      square[choice] = mark;
    
    for (int j =1; j<= a; j++)
    {
      if(square[j] == 'X' or square[j] == 'O' )
      cout<<" "<<square[j]<<"  |";
      else
      {
        cout<<" ";
        if(j>9)
        cout<<j<<" |";
        if(j<=9)
        cout<<j<<"  |"; 
      } 

        
      if(j % n == 0)
          cout<<endl<<endl;
          
    }
    // end of print board 


    // check all winning conditions
    // checking Horizontal rows
     for(int j =0; j<n; j++)
    {
         int count = 0;
        for( int i = 1+(j*n); i<=n+(j*n); i++)
        {
            if( square[i] ==square[i+1])
            {
                if(square[i] == 'X' || square[i] == 'O')
                {
                  count = count +  1;
                }
            }
       
        }
        if(count == n-1)
        {
            return 1;
        }
    }

    // checking Vertical rows
    for(int j = 0; j<n; j++)
    {
        int count = 0;
        for( int i = 1+j; i<=(n*n); i = i+n)
        {
           if(square[i] =='X' || square[i] == 'O')
           {
            if( square[i] ==square[i+n])

                count = count +  1;
           }
     
        }
      if(count == n-1)
      {
        return 1;
      }
   
    }

    // checking diagonal conditions
    int count = 0;
    for(int i = 1; i<=n*n; i = i + n+1)
    {
      if(square[i] =='X' || square[i] == 'O')
      {
        if(square[i] == square[i+n+1] )
            count = count + 1;
      }
    }
    if(count == n-1)
    {
        return 1;
    }
  
    int count1 = 0;
    for(int i = n; i<=n*n; i = i + n-1)
    {
      if(square[i] == 'X' || square[i] == 'O')
      {
        if(square[i] == square[i+n-1] )
            count1 = count1 + 1;
      }
    }
    if(count1 == n-1)
    {
        return 1;
    }    

    int cnt = 0;
    for(int x =0; x<=n; x++)
    {
      if(square[x] == 'X' || square[x] == 'O')
        cnt++;
      if(cnt == n)
        return 0;
      else
        return -1;

      
    }

}

